ROOT_DIR = $(shell pwd)
BIN_DIR = $(ROOT_DIR)/bin
O = $(ROOT_DIR)/obj
LIB_DIR = $(ROOT_DIR)/lib
SRC_DIR = $(ROOT_DIR)/src
INCLUDE_DIR = $(ROOT_DIR)/include
export ROOT_DIR BIN_DIR O LIB_DIR SRC_DIR INCLUDE_DIR

CC = gcc 
RM = rm -f
CPPFLAGS = -Wall -Wextra -ansi -pedantic -I$(INCLUDE_DIR)
CFLAGS = -g -c 
LDFLAGS = -Wl,-rpath $(LIB_DIR) -L$(LIB_DIR)
LIBS = 

EXE = $(BIN_DIR)/a.out

OBJS = \
	   $(O)/solver.o	\
	   $(O)/forwardChecking.o	\
	   $(O)/backtracking.o	\
	   $(O)/grid.o	\
	   $(O)/main.o	\


all: $(EXE)

$(EXE): $(OBJS)
	@mkdir -p $(BIN_DIR)
	$(CC) $^ -o $(EXE) $(LDFLAGS) $(LIBS)

$(O)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $< -o $@



clean:
	@$(RM) $(O)/*.o

dist-clean:
	@make clean
	@$(RM) $(BIN_DIR)/*


.PHONY: all build clean dist-clean build

