/**
 * @file include/solver.h
 *
 * Sudoku solver.
 */

#ifndef __SOLVER_H
#define __SOLVER_H

#include <grid.h>

#define DEFAULT_SOLVING_METHOD 1
#define NB_METHODS 2

typedef struct _Solver{
    sudokugrid_t grid;
    char *gridName;
    int methodId;
    char *methodName;
    int solved;
    int defaultMethod;
    FILE *out;
}solver_t;

/**
 * Initializes a new solver.
 *
 * @param solver The solver.
 */
void solver_init(solver_t *solver);

/**
 * Frees previously allocated blocks in the solver.
 *
 * @param solver The solver.
 */
void solver_free(solver_t *solver);

/**
 * Set the solver puzzle.
 *
 * @param solver The solver.
 * @param filepath Path to the sudoku.
 */
void solver_set_puzzle(solver_t *solver, char *filepath);

/**
 * Select the method to solve the puzzle.
 *
 * @param solver The sudoku solver.
 */
void solver_select_method(solver_t *solver, char *method);

/**
 * Solves the sudoku grid contained in the solver.
 *
 * @param solver The solver.
 *
 * @return 1 if the puzzle is solved, 0 otherwise.
 */
int solver_solve(solver_t *solver);

/**
 * Outputs the results after solving a puzzle.
 *
 * @param solver The solver.
 * @param pretty Set to 1 to toggle pretty display, 0 otherwise.
 */
void solver_output_results(solver_t solver, int pretty);

/**
 * Sets solver output file. Default being stdout.
 *
 * @param solver The solver.
 * @param filepath Path to the output file.
 */
void solver_set_output(solver_t *solver, char *filepath);

#endif


