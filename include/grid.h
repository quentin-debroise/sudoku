/**
 * @file include/grid.h
 *
 * Helper function to handle sudoku grids
 */

#ifndef __GRID_H
#define __GRID_H

#define DEFAULT_SIZE 9
#define DEFAULT_EMPTY_CHAR '.'


typedef struct _SudokuGrid{
    int **grid;
    int size;
    int squareSize;
    float solvedTime;
    int nbExploredStates;
}*sudokugrid_t;

/**
 * Creates a new sudokugrid_t.
 *
 * @param size Size of the sudoku.
 *
 * @return The newly allocated grid structure. Must be freed by the user!
 * If an error occurs, returns NULL.
 */
sudokugrid_t grid_create(int size);

/**
 * Destroys a sudoku grid previously created with grid_create().
 *
 * @param grid The grid to destroy.
 */
void grid_destroy(sudokugrid_t grid);

/**
 * Parses a grid read from a text file.
 *
 * @param filename Name of the file containing the sudoku grid.
 *
 * @return An array of integers representing the grid. 0 means an empty cell.
 */
sudokugrid_t parse_grid(char *filename);

/**
 * Checks if a sudoku row is valid.
 *
 * @param grid Sudoku grid.
 * @param newDigit New digit added to the row to check.
 * @param row Row to check.
 * @param size Size of the sudoku.
 *
 * @return 1 if the row is valid, 0 otherwise.
 */
int is_valid_row(sudokugrid_t grid, int newDigit, int row);

/**
 * Checks if a sudoku column is valid.
 *
 * @param grid Sudoku grid.
 * @param newDigit New digit added to the column to check.
 * @param col Column to check.
 * @param size Size of the sudoku.
 *
 * @return 1 if the column is valid, 0 otherwise.
 */
int is_valid_col(sudokugrid_t grid, int newDigit, int col);

/**
 * Checks if a sudoku sqaure is valid.
 *
 * @param grid Sudoku grid.
 * @param row Row of the newly added digit.
 * @param col Column of the newly added digit.
 * @param squareSize Size of a sudoku inner square.
 *
 * @return 1 if the square is valid, 0 otherwise.
 */
int is_valid_square(sudokugrid_t grid, int row, int col);

/**
 * Checks if the added digit is valid.
 *
 * @param grid The sudoku grid.
 * @param row Row of the added digit.
 * @param col Column of the added digit.
 * @param size Size of the sudoku grid.
 *
 * @return 1 if the new digit is valid, 0 otherwise.
 */
int is_valid_digit(sudokugrid_t grid, int row, int col);

/**
 * Displays a sudoku grid.
 *
 * @param grid Sudoku grid.
 */
void grid_display(sudokugrid_t grid, FILE *out);

/**
 * Pretty displays for a sudoku grid.
 *
 * @param grid Sudoku grid.
 */
void grid_pretty_display(sudokugrid_t grid, FILE *out);

#endif


