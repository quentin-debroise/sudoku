/**
 * @file include/backtracking.h
 *
 * Sudoku solver using backtracking algorithm.
 * i.e : Deep First Search
 */

#ifndef __BACKTRACKING_H
#define __BACKTRACKING_H

#include <grid.h>

/**
 * Solves a sudoku grid using backtracking.
 *
 * @param grid Sudoku grid.
 *
 * @return 1 if a valid solution has been found, 0 otherwise.
 */
int solve_backtracking(sudokugrid_t grid);

/*
 * Finds the next empty cell. 
 * 
 * @param grid Sudoku grid.
 * @param currentRow Current row.
 * @param currentCol Current column.
 * @param nextRow [OUT] Row of the next empty cell.
 * @param nextCol [OUT] Column of the next exmpty cell.
 */
void get_next_empty_cell(
        sudokugrid_t grid,
        int currentRow,
        int *nextRow, int *nextCol);

#endif


