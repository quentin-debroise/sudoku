/**
 * @file include/forwardChecking.h
 *
 * Forward Checking algorithm to solve sudoku puzzle.
 */

#ifndef __FORWARDCHECKING_H
#define __FORWARDCHECKING_H

/*
 * Uses bits to represents the domain values
 * 111111111 -> 9 bits for 9 numbers
 * 111111101 -> The value 2 isn't available anymore.
 * 000000000 -> no possible value for the cell. Or value already set.
 */

#include <grid.h>

typedef struct _DomainsSave{
    int *savedRow;
    int *savedCol;
    int *savedSquare;
}domainsave_t;

/**
 * Solves the sudoku using forward checking algorithm.
 *
 * @param grid The sudoku grid.
 *
 * @return 1 if the puzzle is solved, 0 otherwise.
 */
int solve_forward_checking(sudokugrid_t grid);

/**
 * Propagate when a new value is added in the grid.
 *
 * @param grid The sudoku grid.
 * @param domains Domains.
 * @param row Row of the new digit.
 * @param col Column of the new digit.
 */
void propagate(sudokugrid_t grid, int **domains, int row, int col);

/**
 * Creates the domains array.
 *
 * @param size Size of the sudoku.
 *
 * @return The domains initialized with all possible values.
 */
int** create_domains(int size);

/**
 * Initialize the domains using the existing numbers
 * on the sudoku grid.
 *
 * @param grid The sudoku grid.
 * @param domains The domains.
 */
void init_domains(sudokugrid_t grid, int **domains);

/**
 * Finds next best cell to fill in.
 * Uses heuristic : next cell is the one with the less remaining possibilities.
 *
 * @param grid The sudoku grid.
 * @param domains Domains.
 * @param nrow Row of the next cell to fill in.
 * @param ncol Column of the next cell to fill in.
 */
void get_next_cell(sudokugrid_t grid, int **domains, int *nrow, int *ncol);

#endif


