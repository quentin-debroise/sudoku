# Sudoku Solver

Simple Sudoku game solver.

## Features
- [x] Depth First Search solving method
- [x] Forward Checking solving method 
- [ ] Arc Consistency solving method
- [x] Time & explored states information
- [x] Pretty Grid display
- [x] Solves grids of different sizes

## Build & Run
- Download the source code
```
git clone https://framagit.org/quentin-debroise/sudoku.git
```
- Then build the source and run with youre own grids!
```
cd ./sudoku
make
./bin/a.out -g grids/grid1.txt -p
```


## Screenshots
![Screenshot of basic](screenshots/screenshot_1.gif)
![Screenshot of basic](screenshots/screenshot_2.gif)

## Documentation
### Grids format
Grids are available in the grid/ folder. Have a look at how they are formatted!

- First line is the size of the puzzle 3 is regular sudokus (9x9), 2 are small ones (4x4), and 4 are large ones (16x16).
- Each following line represents a line in the puzzle:
	- A value between **1** and **9** (also from a to f for large puzzles) for number already placed in the grid
	- A **.** for representing an empty cell

### Options
- **-g GRID**
specify which grid to solve.
- **-m METHOD**
Specify the method used to solve the sudoku. METHOD being among _dfs_ for Depth First Search or _forwardchecking_ for the Forward Checking algorithm.
- **-p**
display the output grid in a pretty format.
- **-o FILE**
Specify an output file to which results will be written.
## Known bugs
- Feeding in an empty grid using Forward Checking method will crash

## License
It's just a fun little project of mine. You're free to do whatever you want with it.
