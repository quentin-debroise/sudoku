/**
 * @file src/backtracking.c
 *
 * Sudoku solver using backtracking algorithm.
 * i.e : Deep First Search
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <backtracking.h>

/**
 * Runs the recursive backtracking algorithm.
 *
 * @param grid The sudoku grid.
 * @param row Row of the cell to modify.
 * @param col Column of the cell to modify.
 * @param size Size of the sudoku grid.
 *
 * @return 1 if the grid is solved, 0 otherwise.
 */
static int _backtrack(sudokugrid_t grid, int row, int col);


int solve_backtracking(sudokugrid_t grid){
    int row, col;
    int res;
    clock_t tic, toc;
    grid->nbExploredStates = 0;

    tic = clock();
    get_next_empty_cell(grid, 0, &row, &col);
    res = _backtrack(grid, row, col);
    toc = clock();

    grid->solvedTime = (float)(toc - tic) / CLOCKS_PER_SEC;
    return res;
}

void get_next_empty_cell(
        sudokugrid_t grid,
        int currentRow,
        int *nextRow, int *nextCol){
    int i, j;

    for(i = currentRow; i < grid->size; i++){
        for(j = 0; j < grid->size; j++){
            if(!grid->grid[i][j]){
                *nextRow = i;
                *nextCol = j;
                return;
            }
        }
    }

    *nextRow = -1;
    *nextCol = -1;
}


int _backtrack(sudokugrid_t grid, int row, int col){
    int i;
    int nrow, ncol;

    for(i = 0; i < grid->size; i++){
        grid->grid[row][col]++;
        grid->nbExploredStates++;
        if(is_valid_digit(grid, row, col)){
            get_next_empty_cell(grid, row, &nrow, &ncol);
            if(nrow == -1 && ncol == -1){
                return 1;
            }
            if(_backtrack(grid, nrow, ncol)){
                return 1;
            }
        }
    }

    /* Reset & Backtrack */
    grid->grid[row][col] = 0;
    return 0;
}


