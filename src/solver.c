/**
 * @file src/solver.c
 *
 * Sudoku solver.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <backtracking.h>
#include <forwardChecking.h>

#include <solver.h>

/**
 * List of all possible solving methods.
 */
static const char *methodsNames[NB_METHODS] = {"dfs", "forwardchecking"};
int (* const solvingFunctions[2])(sudokugrid_t) = {
    solve_backtracking,
    solve_forward_checking
};


void solver_init(solver_t *solver){
    solver->grid = NULL;
    solver->gridName = NULL;
    solver->methodName = NULL;
    solver->out = stdout;
    solver->solved = 0;
    solver->methodId = DEFAULT_SOLVING_METHOD;
}

void solver_free(solver_t *solver){
    fclose(solver->out);
    free(solver->gridName);
    free(solver->methodName);
    grid_destroy(solver->grid);
}

void solver_set_puzzle(solver_t *solver, char *filepath){
    grid_destroy(solver->grid);
    free(solver->gridName);

    solver->gridName = malloc(strlen(filepath) * sizeof(char));
    strcpy(solver->gridName, filepath);
    solver->grid = parse_grid(filepath);
}

void solver_select_method(solver_t *solver, char *method){
    int i;
    int exists = 0;

    if(!method){
        return;
    }

    for(i = 0; i < NB_METHODS; i++){
        if(strcmp(methodsNames[i], method) == 0){
            solver->methodId = i;
            exists = 1;
            break;
        }
    }

    if(exists){
        solver->defaultMethod = 0;
        solver->methodName = malloc(strlen(method) * sizeof(char));
        strcpy(solver->methodName, method);
    }
    else{
        fprintf(stderr, "NOTICE: Method %s doesn't exist\n", method);
        fprintf(stderr, "NOTICE: Using default...\n");
    }
}

int solver_solve(solver_t *solver){
    solver->solved = solvingFunctions[solver->methodId](solver->grid);
    return solver->solved;
}

void solver_output_results(solver_t solver, int pretty){
    fprintf(solver.out, "[Grid]\n%s\n", solver.gridName);
    if(solver.defaultMethod){
        fprintf(solver.out, "[Method]\nforwardchecking (default)\n");
    }
    else{
        fprintf(solver.out, "[Method]\n%s\n", solver.methodName);
    }

    if(solver.solved){
        fprintf(solver.out, "[Solution]\n");
        if(pretty){
            grid_pretty_display(solver.grid, solver.out);
        }
        else{
            grid_display(solver.grid, solver.out);
        }
        fprintf(solver.out, "[SolutionTime]\n%.4f sec\n", solver.grid->solvedTime);
        fprintf(solver.out, "[ExploredStates]\n%d\n", solver.grid->nbExploredStates);
    }
    else{
        fprintf(solver.out, "NO SOLUTION FOUND!\n");
    }
}

void solver_set_output(solver_t *solver, char *filepath){
    solver->out = fopen(filepath, "w");
    if(!solver->out){
        fprintf(stderr, "NOTICE: Can't open %s\n", filepath);
        fprintf(stderr, "NOTICE: Using default output: stdout\n");
    }
}
