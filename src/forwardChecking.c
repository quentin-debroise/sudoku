/**
 * @file src/forwardChecking.c
 *
 * Forward Checking algorithm to solve sudoku puzzle.
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <forwardChecking.h>

/**
 * Propagates the adding of a new value. Update domains for cells in
 * the row of the cell where the value was added.
 *
 * @param domains Domains array.
 * @param newDigit New value added to the grid.
 * @param row Row where to propagate.
 * @param size Size of the sudoku grid.
 */
static void _propagate_row(int **domains, int newDigit, int row, int size);
/**
 * Propagates the adding of a new value. Update domains for cells in
 * the column of the cell where the value was added.
 *
 * @param domains Domains array.
 * @param newDigit New value added to the grid.
 * @param col Column where to propagate.
 * @param size Size of the sudoku grid.
 */
static void _propagate_col(int **domains, int newDigit, int col, int size);
/**
 * Propagates the adding of a new value. Update domains for cells in
 * the inner square of the cell where the value was added.
 *
 * @param domains Domains array.
 * @param newDigit New value added to the grid.
 * @param row Row. 
 * @param col Column.
 * @param squareSize Size of an inner square.
 */
static void _propagate_square(
        int **domains,
        int newDigit,
        int row, int col,
        int squareSize);
/**
 * Runs the recursive forward checking algorithm.
 *
 * @param grid The sudoku grid.
 * @param domains The domains.
 * @param row Row of the cell to modify.
 * @param col Column of the cell to modify.
 *
 * @return 1 if the grid is solved, 0 otherwise.
 */
static int _forward_checking(sudokugrid_t grid, int **domains, int row, int col);
/**
 * Counts the number of remaining possible values for a cell.
 *
 * @param domain Domain representation with bits.
 * @param size Size of the puzzle, thus size of the domain.
 *
 * @return The number of remaining possibilities.
 */
static int _count_remaining_possibilities(int domain, int size);
/**
 * Save the domains potentially modified by the current cell.
 *
 * @param domains Domains.
 * @param domsave [OUT] Save of the domains.
 * @param squareSize Size of an inner square.
 */
static void _save_domains(int **domains, domainsave_t *domsave, int row, int col, int squareSize);
/**
 * Restore the domains potentially modified by the current cell.
 *
 * @param domains Domains.
 * @param domsave [OUT] Save of the domains.
 * @param squareSize Size of an inner square.
 */
static void _restore_domains(int **domains, domainsave_t domsave, int row, int col, int squareSize);


int solve_forward_checking(sudokugrid_t grid){
    int row, col;
    int res;
    clock_t tic, toc;
    int **domains = create_domains(grid->size);
    init_domains(grid, domains);

    grid->nbExploredStates = 0;
    tic = clock();
    get_next_cell(grid, domains, &row, &col);
    res = _forward_checking(grid, domains, row, col);
    toc = clock();
    grid->solvedTime = (float)(toc - tic) / CLOCKS_PER_SEC;

    free(*domains);
    free(domains);
    return res;
}

int** create_domains(int size){
    int i;
    int **domains;
    int nbcells = size * size;

    domains = malloc(size * sizeof(int*));
    *domains = malloc(size * size * sizeof(int));
    for(i = 0; i < size; i++){
        domains[i] = *domains + i * size;
    }

    for(i = 0; i < nbcells; i++){
        *(*domains + i) = -1; /* 9 possible values, all bits set 111111111 */
    }

    return domains;
}

void init_domains(sudokugrid_t grid, int **domains){
    int i, j;

    for(i = 0; i < grid->size; i++){
        for(j = 0; j < grid->size; j++){
            if(grid->grid[i][j]){
                domains[i][j] = 0;
                propagate(grid, domains, i, j);
            }
        }
    }
}

void propagate(sudokugrid_t grid, int **domains, int row, int col){
    int newDigit = grid->grid[row][col];
    _propagate_row(domains, newDigit, row, grid->size);
    _propagate_col(domains, newDigit, col, grid->size);
    _propagate_square(domains, newDigit, row, col, grid->squareSize);
}

void get_next_cell(sudokugrid_t grid, int **domains, int *nrow, int *ncol){
    int i, j;
    int leftPossibilities = grid->size;
    int currentPossibilities;
    int found = 0;
    /**nrow = 6; *ncol = 6; [> Case of an empty grid <]*/

    for(i = 0; i < grid->size; i++){
        for(j = 0; j < grid->size; j++){
            if(grid->grid[i][j]){
                continue;
            }
            found = 1;
            currentPossibilities = _count_remaining_possibilities(
                    domains[i][j],
                    grid->size);
            if(currentPossibilities < leftPossibilities){
                leftPossibilities = currentPossibilities;
                *nrow = i;
                *ncol = j;
            }
            if(leftPossibilities == 1){
                return;
            }
        }
    }

    if(!found){
        *nrow = -1;
        *ncol = -1;
    }
}


void _propagate_row(int **domains, int newDigit, int row, int size){
    int i;
    int cleanbit = ~(1 << (newDigit - 1));

    for(i = 0; i < size; i++){
        domains[row][i] &= cleanbit; 
    }
}

void _propagate_col(int **domains, int newDigit, int col, int size){
    int i;
    int cleanbit = ~(1 << (newDigit - 1));

    for(i = 0; i < size; i++){
        domains[i][col] &= cleanbit; 
    }
}

void _propagate_square(int **domains, int newDigit, int row, int col, int squareSize){
    int i, j;
    int crow = squareSize * (int)(row / squareSize);
    int ccol = squareSize * (int)(col / squareSize);
    int cleanbit = ~(1 << (newDigit - 1));

    for(i = 0; i < squareSize; i++){
        for(j = 0; j < squareSize; j++){
            domains[crow + i][ccol + j] &= cleanbit;
        }
    }
}

int _forward_checking(sudokugrid_t grid, int **domains, int row, int col){
    int i;
    int nrow, ncol;
    int ret = 0;
    int fcres;
    domainsave_t domsave;
    domsave.savedRow = malloc(grid->size * sizeof(int));
    domsave.savedCol = malloc(grid->size * sizeof(int));
    domsave.savedSquare = malloc(grid->size * sizeof(int));

    for(i = 0; i < grid->size; i++){
        if(!((domains[row][col] >> i) & 1)){
            continue;
        }
        grid->grid[row][col] = i + 1;
        grid->nbExploredStates++;
        if(is_valid_digit(grid, row, col)){
            get_next_cell(grid, domains, &nrow, &ncol);
            if(nrow == -1 && ncol == -1){
                ret = 1;
                break;
            }

            _save_domains(domains, &domsave, row, col, grid->squareSize);
            propagate(grid, domains, row, col);
            fcres = _forward_checking(grid, domains, nrow, ncol);
            _restore_domains(domains, domsave, row, col, grid->squareSize);
            if(fcres){
                ret = 1;
                break;
            }
        }
    }
    free(domsave.savedRow);
    free(domsave.savedCol);
    free(domsave.savedSquare);

    if(ret){
        return 1;
    }

    /* Reset & Backtrack */
    grid->grid[row][col] = 0;
    return 0;
}

int _count_remaining_possibilities(int domain, int size){
    int i;
    int res = 0;
    for(i = 0; i < size; i++){
        res += (domain >> i) & 1;
    }

    return res;
}

void _save_domains(int **domains, domainsave_t *domsave, int row, int col, int squareSize){
    int i;
    int size = squareSize * squareSize;
    int crow = squareSize * (int)(row / squareSize);
    int ccol = squareSize * (int)(col / squareSize);

    for(i = 0; i < size; i++){
        domsave->savedRow[i] = domains[row][i];
        domsave->savedCol[i] = domains[i][col];
        domsave->savedSquare[i] = domains[crow + i / squareSize][ccol + i % squareSize];
    }
}

void _restore_domains(int **domains, domainsave_t domsave, int row, int col, int squareSize){
    int i;
    int size = squareSize * squareSize;
    int crow = squareSize * (int)(row / squareSize);
    int ccol = squareSize * (int)(col / squareSize);

    for(i = 0; i < size; i++){
        domains[row][i] = domsave.savedRow[i];
        domains[i][col] = domsave.savedCol[i];
        domains[crow + i / squareSize][ccol + i % squareSize] = domsave.savedSquare[i];
    }
}
