/**
 * @file src/grid.c
 *
 * Helper function to handle sudoku grids
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <grid.h>

/*
 * Parses the file.
 * 
 * @param f Pointer on the opened file containing the grid.
 * @param grid Allocated grid.
 * @param size Size of the sudoku.
 */
static void _parse(FILE *f, sudokugrid_t grid);
/*
 * Displays a line which length depends on the puzzle's size
 *
 * @param size Size of the sudoku.
 */
static void _disp_line(int size, FILE *out);


sudokugrid_t grid_create(int squareSize){
    int i;

    sudokugrid_t grid = malloc(sizeof(struct _SudokuGrid));
    grid->size = squareSize * squareSize;
    grid->squareSize = squareSize;
    grid->solvedTime = 0;
    grid->nbExploredStates = 0;

    grid->grid = malloc(grid->size * sizeof(int*));
    *(grid->grid) = malloc(grid->size * grid->size * sizeof(int));
    for(i = 0; i < grid->size; i++){
        grid->grid[i] = *(grid->grid) + i * grid->size;
    }

    return grid;
}

void grid_destroy(sudokugrid_t grid){
    if(grid){
        free(*(grid->grid));
        free(grid->grid);
        free(grid);
    }
}

sudokugrid_t parse_grid(char *filename){
    sudokugrid_t grid = NULL;
    FILE *f = fopen(filename, "r");
    int squareSize;
    char rc;

    if(!f){
        fprintf(stderr, "Couldn't open %s\n", filename);
        exit(-1);
    }

    fscanf(f, "%d%c", &squareSize, &rc);
    grid = grid_create(squareSize);
    _parse(f, grid);

    fclose(f);
    return grid;
}

int is_valid_row(sudokugrid_t grid, int newDigit, int row){
    int i;
    int found = -1;

    for(i = 0; i < grid->size; i++){
        if(grid->grid[row][i] == newDigit){
            found++;
            if(found == 1){
                return 0;
            }
        }
    }

    return !found;
}

int is_valid_col(sudokugrid_t grid, int newDigit, int col){
    int i;
    int found = -1;

    for(i = 0; i < grid->size; i++){
        if(grid->grid[i][col] == newDigit){
            found++;
            if(found == 1){
                return 0;
            }
        }
    }

    return !found;
}

int is_valid_square(sudokugrid_t grid, int row, int col){
    int i, j;
    int found = -1;
    int crow = grid->squareSize * (int)(row / grid->squareSize);
    int ccol = grid->squareSize * (int)(col / grid->squareSize); 
    int newDigit = grid->grid[row][col];

    for(i = 0; i < grid->squareSize; i++){
        for(j = 0; j < grid->squareSize; j++){
            if(grid->grid[crow + i][ccol + j] == newDigit){
                found++;
                if(found == 1){
                    return 0;
                }
            }
        }
    }

    return !found;
}

int is_valid_digit(sudokugrid_t grid, int row, int col){
    int newDigit = grid->grid[row][col];

    return is_valid_row(grid, newDigit, row) &&
        is_valid_col(grid, newDigit, col) &&
        is_valid_square(grid, row, col);
}

void grid_display(sudokugrid_t grid, FILE *out){
    int i, j;
    for(i = 0; i < grid->size; i++){
        for(j = 0; j < grid->size; j++){
            if(grid->grid[i][j]){
                fprintf(out, "%d", grid->grid[i][j]);
            }
            else{
                fprintf(out, ".");
            }
        }
        fprintf(out, "\n");
    }
}

void grid_pretty_display(sudokugrid_t grid, FILE *out){
    int i, j;
    for(i = 0; i < grid->size; i++){
        if(!(i % grid->squareSize)){
            _disp_line(grid->squareSize, out);
        }
        for(j = 0; j < grid->size; j++){
            if(!(j % grid->squareSize)){
                fprintf(out, "|");
            }
            (!grid->grid[i][j]) ?
                fprintf(out, " %c ", DEFAULT_EMPTY_CHAR) :
                fprintf(out, " %d ", grid->grid[i][j]);
        }
        fprintf(out, "|\n");
    }
    _disp_line(grid->squareSize, out);
}


void _parse(FILE *f, sudokugrid_t grid){
    int row = 0;
    int digit;
    int i;
    char *buffer = malloc((grid->size + 2) * sizeof(char));

    while(fgets(buffer, grid->size + 2, f)){
        if((int)strlen(buffer) - 1 != grid->size){
            fprintf(stderr, "Wrong format! Sizes don't match\n");
            grid_destroy(grid);
            free(buffer);
            fclose(f);
            exit(-1);
        }
        for(i = 0; i < grid->size; i++){
            digit = buffer[i] == DEFAULT_EMPTY_CHAR ? 0 : buffer[i] - '0';
            grid->grid[row][i] = digit;
        }
        row++;
    }

    free(buffer);
}

void _disp_line(int squareSize, FILE *out){
    int i;
    int n = (squareSize + 2 * squareSize) * squareSize + squareSize - 1;
    fprintf(out, "+");
    for(i = 0; i < n; i++){
        if((i + 1) % (squareSize + 2 * squareSize + 1)){
            fprintf(out, "-");
        }
        else{
            fprintf(out, "+");
        }
    }
    fprintf(out, "+\n");
}
