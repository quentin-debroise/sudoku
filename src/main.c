/**
 * @file src/main.c
 *
 * DESCRIPTION
 */


#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <time.h>
#include <solver.h>


int main(int argc, char *argv[]){
    int opt;
    int prettyDisplay = 0;
    solver_t solver;

    solver_init(&solver);

    while((opt = getopt(argc, argv, "g:m:o:p")) != -1){
        switch(opt){
            case 'g':
                solver_set_puzzle(&solver, argv[optind - 1]);
                break;
            case 'm':
                solver_select_method(&solver, argv[optind - 1]);
                break;
            case 'o':
                solver_set_output(&solver, argv[optind - 1]);
                break;
            case 'p':
                prettyDisplay = 1;
                break;
            default: 
                exit(1);
                break;
        }
    }

    if(!solver.grid){
        fprintf(stderr, "Usage: bin/a.out -g puzzle [-m method] [-o output]\n");
        fprintf(stderr, "Method being among: dfs,forwardchecking(default),\n");
        exit(-1);
    }


    solver_solve(&solver); 
    solver_output_results(solver, prettyDisplay);

    solver_free(&solver);
    return 0;
}

